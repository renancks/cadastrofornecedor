
package model;

/**
 *
 * @author Renan
 */
public class PessoaJuridica extends Fornecedor{
    
    private String cnpj;
    private String razaoSocial;
    
    public PessoaJuridica (String nome, String telefoneFixo, String telefoneCelular, String cnpj) {
	super(nome, telefoneFixo, telefoneCelular);
	this.cnpj = cnpj;
    }
    public PessoaJuridica (){
        
    }
	
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    

}
