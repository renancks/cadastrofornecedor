
package Controller;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

public class ControleFornecedor {

    private ArrayList<Fornecedor> listaFornecedores;
    
    public ControleFornecedor () {
        listaFornecedores = new ArrayList<Fornecedor>();
    }
    
    public void setListaFornecedores(ArrayList<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }
    
    public String adicionar (PessoaFisica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso.";
    }
    
    public String adicionar (PessoaJuridica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso.";
    }
    
    public void remover (PessoaJuridica fornecedor) {
        listaFornecedores.remove(fornecedor);
    }
    
    public void remover (PessoaFisica fornecedor) {
        listaFornecedores.remove(fornecedor);
    }
    
    public Fornecedor pesquisarFornecedor (String umNome) {
        for (Fornecedor umFornecedor : listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) {
                return umFornecedor;
            }
        }
        return null;
    }
}
    
    

